package com.yonghen.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: ZhangYi
 * @Date: 2022/07/24/  22:08
 * @Description:
 */


@Configuration
public class MySelfRule {

    //随机负载均衡策略
    @Bean
    public IRule myRule(){
        return new RandomRule();
    }
}
