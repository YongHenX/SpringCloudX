package com.yonghen.springcloudzy;

import com.yonghen.myrule.MySelfRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

/**
 * @Author: ZhangYi
 * @Date: 2022/07/10/  17:39
 * @Description:
 */

@SpringBootApplication
@EnableEurekaClient
//负载均衡策略采用自定义的方式，不用默认的轮询
//@RibbonClient(name = "CLOUD-PAYMENT-SERVICE", configuration = MySelfRule.class)
public class OrderMain80 {
    public static void main(String[] args) {
        SpringApplication.run(OrderMain80.class, args);
    }
}
