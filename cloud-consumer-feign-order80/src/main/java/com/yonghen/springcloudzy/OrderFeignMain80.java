package com.yonghen.springcloudzy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Author: ZhangYi
 * @Date: 2022/07/25/  22:22
 * @Description:
 */

@SpringBootApplication
//使用并激活Feign
@EnableFeignClients
public class OrderFeignMain80 {
    public static void main(String[] args) {
        SpringApplication.run(OrderFeignMain80.class, args);
    }
}
