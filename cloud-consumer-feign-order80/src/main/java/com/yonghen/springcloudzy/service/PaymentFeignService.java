package com.yonghen.springcloudzy.service;

import com.yonghen.springcloudzy.entities.CommonResult;
import com.yonghen.springcloudzy.entities.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @Author: ZhangYi
 * @Date: 2022/07/25/  22:25
 * @Description:
 */

@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE")
public interface PaymentFeignService {
    //将需要调用微服务的controller中的方法直接写在这里，
    // 只用写方法名和参数，不用管具体实现
    @GetMapping(value = "/payment/get/{id}")
    CommonResult<Payment> getPaymentById(@PathVariable("id") Long id);

    @GetMapping(value = "/payment/feign/timeout")
    String paymentFeignTimeout();
}
